# Introduction

The goal of this repository is to provide a roadmap, easily understandable by anyone.
You can see the current progress of the project as well as the future features planned.

## Organisation

Each 'issue' represent a feature or a components required by one or many features

### Tags
| Tag | Description |
|--|--|
| Feature | A feature available to one or multiple actors |
|Core|Core component, required by one or many features|
|v1, v2, ...|The version for which this feature will be available|
|Smart contract|Smart contract, on elrond / Multiversx only atm|
|Dapp|Smart contract 'frontend' / user interface|

### Actors

| Actor | Description |
|--|--|
| Administrator | System-1337 team member with privilegied access |
| Project administrator | Anyone responsible for a project in partnership with System-1337 |
| User / Community member | End user |




